//Declare les variables
var mots = ["grille","voiture","ventilateur"];
var mot_a_deviner = mots [Math.floor(Math.random() * mots.length)];
var lettres_trouvees = [];
var wrong_letter = [];
var wrong = 10;

var letter_submit = function() {
   var letter = document.getElementById("letter").value;
   
   //Message d'erreur
   if(lettres_trouvees.indexOf(letter) != -1) {
      document.getElementById("error").innerHTML = "Vous avez déjà joué cette lettre! :D";
   } else {
      document.getElementById("error").innerHTML = "";
   }
      
   //La lettre est présente dans le mot
   if(mot_a_deviner.indexOf(letter) != -1) {
      lettres_trouvees.push(letter); 
      
      //reset des compteurs
      var lol = 0;
      mot_en_cours.innerHTML = "";
      
      //boucle qui affiche le mot
      for(var i=0;i<mot_a_deviner.length; i++) {
         if(lettres_trouvees.indexOf(mot_a_deviner[i]) != -1) {
            mot_en_cours.innerHTML += " " + mot_a_deviner[i] + " ";
            lol++;
         } else {
            mot_en_cours.innerHTML += " _ ";
         }
      }
      
      //Fin de partie
      if (lol==mot_a_deviner.length) {
         document.getElementById("game").innerHTML = "Vous avez gagné !";
      }
      
      
   //La lettre n'est pas dans le mot
   } else {
   
      //Compteur d'erreurs + message d'erreur
      if(wrong_letter.indexOf(letter) != -1) {
         document.getElementById("error").innerHTML = "Vous avez déjà joué cette lettre! :)";
      } else {
         alert("lettre " + letter + " non trouvée");
         wrong--;
         wrong_letter.push(letter);
         document.getElementById("error").innerHTML = "";
      }
      
      document.getElementById("compteur").innerHTML = "Essais restant : "+wrong;
      document.getElementById("wrong_letter").innerHTML = "Lettres éronné : "+wrong_letter;
      
      //Fin de partie
      if (wrong==0) {
         document.getElementById("game").innerHTML = "Vous avez perdu !";
      }
   }

}


