<!DOCTYPE html>
<html>
 <head>
   <meta charset="utf-8">
   <title>Pendu en JavaScript</title>
   <link rel="stylesheet" type="text/css" href="bootstrap.css">
   <link rel="stylesheet" type="text/css" href="style.css">
   <script type="text/javascript" src="javascript.js"></script>
 </head>
 <body>
   <div class="container">
      <h1>Jeu de pendu</h1>
      <div class="row">
         <div class="col-md-12">
            <div id="mot_en_cours"></div>
         </div>
      </div>
      <div class="row">
         <div id="game" class="col-md-offset-2 ">
            <div class="row">
               <div class="col-md-6 form-inline">
                  <label for="form-control">Lettre :</label>
                  <input type="text" size="1" class="form-control" tabindex="1" id="letter" onchange="letter_submit()">
               </div>
               <div class="col-md-4">
                  <p id="compteur">Essais restant : 10</p>
               </div>
            </div>
            <div class="row wrong">
               <div class="col-md-6">
                  <p id="wrong_letter"></p>
               </div>
               <div class="col-md-4">
                  <p id="error"></p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script>
     var mot_en_cours = document.getElementById("mot_en_cours");
     for(var i=0;i<mot_a_deviner.length; i++) {
       mot_en_cours.innerHTML += " _ ";
     }
   </script>
 </body>
</html>
